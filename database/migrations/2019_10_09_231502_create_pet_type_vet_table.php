<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetTypeVetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pet_type_vet', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pet_type_id')->unsigned();
            $table->foreign('pet_type_id')->references('id')->on('pet_types');
            $table->bigInteger('vet_id')->unsigned();
            $table->foreign('vet_id')->references('id')->on('vets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pet_type_vet');
    }
}
