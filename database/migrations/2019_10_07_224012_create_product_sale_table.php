<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sale', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product')->unsigned();
            $table->foreign('product')->references('id')->on('products');
            $table->bigInteger('sale')->unsigned();
            $table->foreign('sale')->references('id')->on('sales');
            $table->bigInteger('quantity')->unsigned();
            $table->decimal('unit_value', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sale');
    }
}
