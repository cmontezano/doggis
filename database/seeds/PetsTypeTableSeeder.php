<?php

use Illuminate\Database\Seeder;

class PetsTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pet_types')->insert([
            'label' => 'Cães',
            'created_at' => now(),
        ]);

        DB::table('pet_types')->insert([
            'label' => 'Gatos',
            'created_at' => now(),
        ]);

        DB::table('pet_types')->insert([
            'label' => 'Pássaros',
            'created_at' => now(),
        ]);
    }
}
