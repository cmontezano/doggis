<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'admin',
            'label' => 'Administrador',
        ]);

        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'atendente',
            'label' => 'Atendente',
        ]);

        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'cliente',
            'label' => 'Cliente',
        ]);

        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'veterinario',
            'label' => 'Veterinário',
        ]);
    }
}
