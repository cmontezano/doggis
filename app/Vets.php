<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vets extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function petTypes()
    {
        return $this->hasMany(PetTypeVet::class, 'vet_id');
    }
}
