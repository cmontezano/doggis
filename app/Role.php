<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMINISTRADOR = 1;
    const ATENDENTE = 2;
    const CLIENTE = 3;
    const VETERINARIO = 4;

    protected $guarded = [];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
