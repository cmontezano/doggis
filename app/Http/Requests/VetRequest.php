<?php

namespace App\Http\Requests;

use App\Vets;
use App\User;
use Illuminate\Validation\Rule;

class VetRequest extends UserRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        parent::authorize();
        return auth()->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                'cpf' => [
                    'required',
                    Rule::unique((new Vets())->getTable())->ignore($this->route()->vet->user->id ?? null),
                    'numeric'
                ],

            ],
            parent::rules()
        );
    }

    public function messages()
    {
        return array_merge(
            [
                'cpf.unique' => 'Este CPF já está sendo utilizado por outro veterinário.',
                'cpf.numeric' => 'O campo CPF deve ser preenchido apenas com números.'
            ],
            parent::messages()
        );
    }
}
