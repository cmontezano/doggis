<?php

namespace App\Http\Requests;

use App\Customers;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerRequest extends UserRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        parent::authorize();
        return auth()->user()->isAtendente();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(
            [
                'cpf' => [
                    'required',
                    Rule::unique((new Customers())->getTable())->ignore($this->route()->customer->user->id ?? null),
                    'numeric'
                ],

            ],
            parent::rules()
        );
    }

    public function messages()
    {
        return array_merge(
            [
                'cpf.unique' => 'Este CPF já está sendo utilizado por outro cliente.',
                'cpf.numeric' => 'O campo CPF deve ser preenchido apenas com números.'
            ],
            parent::messages()
        );
    }
}
