<?php

namespace App\Http\Controllers;

use App\Http\Requests\VetRequest;
use App\PetTypes;
use App\PetTypeVet;
use App\Role;
use App\User;
use App\Vets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class VetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Vets $model)
    {
        return view('vets.index', ['vets' => $model->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VetRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = Role::VETERINARIO;
        $user->created_at = now();
        $user->save();

        $vet = new Vets();
        $vet->identity_number = $request->identity_number;
        $vet->cpf = $request->cpf;
        $vet->board_registration = $request->board_registration;
        $vet->user_id = $user->id;
        $vet->save();

        if ($request->pets_type_id) {
            foreach ($request->pets_type_id as $petTypeId) {
                $petType = new PetTypeVet();
                $petType->pet_type_id = $petTypeId;
                $petType->vet_id = $vet->id;
                $petType->save();
            }
        }

        return redirect()->route('vet.index')->withStatus(__('Veterinário criado com sucesso.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vets  $vets
     * @return \Illuminate\Http\Response
     */
    public function show(Vets $vets)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vets  $vet
     * @return \Illuminate\Http\Response
     */
    public function edit(Vets $vet)
    {
        return view('vets.edit', compact('vet'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vets  $vets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vets $vet)
    {
        $vet->user->name = $request->name;
        $vet->user->email = $request->email;
        $vet->user->save();
        $vet->identity_number = $request->identity_number;
        $vet->cpf = $request->cpf;
        $vet->board_registration = $request->board_registration;
        $vet->save();

        $vet->petTypes()->delete();
        if ($request->pets_type_id) {
            foreach ($request->pets_type_id as $petTypeId) {
                $petType = new PetTypeVet();
                $petType->pet_type_id = $petTypeId;
                $petType->vet_id = $vet->id;
                $petType->save();
            }
        }

        return redirect()->route('vet.index')->withStatus(__('Veterinário atualizado com sucesso.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vets  $vets
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vets $vet)
    {
        $user = $vet->user;
        $vet->delete();
        $user->delete();

        return redirect()->route('vet.index')->withStatus(__('Veterinário removido com sucesso.'));
    }
}
