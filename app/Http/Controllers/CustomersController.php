<?php

namespace App\Http\Controllers;

use App\Customers;
use App\Http\Requests\CustomerRequest;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Customers $model)
    {
        return view('customers.index', ['customers' => $model->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role_id = Role::CLIENTE;
        $user->save();

        $customer = new Customers();
        $customer->identity_number = $request->identity_number;
        $customer->cpf = $request->cpf;
        $customer->user_id = $user->id;
        $customer->pataz = $request->pataz;
        $customer->save();

        return redirect()->route('customer.index')->withStatus(__('Cliente criado com sucesso.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function show(Customers $customers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customers  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customers $customer)
    {
        return view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customers  $customers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customers $customer)
    {
        $customer->user->name = $request->name;
        $customer->user->email = $request->email;
        $customer->user->save();
        $customer->identity_number = $request->identity_number;
        $customer->cpf = $request->cpf;
        $customer->pataz = $request->pataz;
        $customer->save();

        return redirect()->route('customer.index')->withStatus(__('Cliente atualizado com sucesso.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customers  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customers $customer)
    {
        $user = $customer->user;
        $customer->delete();
        $user->delete();

        return redirect()->route('customer.index')->withStatus(__('Cliente removido com sucesso.'));
    }
}
