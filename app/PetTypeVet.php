<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetTypeVet extends Model
{
    protected $table = 'pet_type_vet';
    protected $guarded = [];

    public function petType()
    {
        return $this->belongsTo(PetTypes::class);
    }
}
