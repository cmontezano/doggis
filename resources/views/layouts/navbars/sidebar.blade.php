<div class="sidebar" data-color="purple" data-background-color="white"
     data-image="{{ asset('material') }}/img/sidebar-1.jpg">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo">
        <a href="https://creative-tim.com/" class="simple-text logo-normal">
            {{ __('Petshop Doggis') }}
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('home') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Página Inicial') }}</p>
                </a>
            </li>


            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('profile.edit') }}">
                    <i class="material-icons">account_box</i>
                    <p>{{ __('Perfil de usuário') }}</p>
                </a>
            </li>

            @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
                <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('user.index') }}">
                        <i class="material-icons">group</i>
                        <p>{{ __('Usuários do sistema') }}</p>
                    </a>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->isAtendente())
                <li class="nav-item{{ $activePage == 'customers' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('customer.index') }}">
                        <i class="material-icons">person</i>
                        <p>{{ __('Clientes') }}</p>
                    </a>
                </li>
            @endif

            @if(\Illuminate\Support\Facades\Auth::user()->isAdmin())
                <li class="nav-item{{ $activePage == 'vets' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('vet.index') }}">
                        <i class="material-icons">pets</i>
                        <p>{{ __('Veterinários') }}</p>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
