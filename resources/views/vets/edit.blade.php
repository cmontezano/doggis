@extends('layouts.app', ['activePage' => 'vets', 'titlePage' => __('Edição de veterinário')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('vet.update', $vet) }}" autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('put')

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Editar veterinário') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('vet.index') }}"
                                           class="btn btn-sm btn-primary">{{ __('Voltar para lista') }}</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Nome') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Nome') }}" value="{{ old('name', $vet->user->name) }}"
                                                   required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" id="input-email" type="email"
                                                   placeholder="{{ __('Email') }}" value="{{ old('email', $vet->user->email) }}" required/>
                                            @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger"
                                                      for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('CPF') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}"
                                                   name="cpf" id="input-cpf" type="text" min="11" max="11"
                                                   placeholder="{{ __('CPF') }}" value="{{ old('cpf', $vet->cpf) }}" required/>
                                            @if ($errors->has('cpf'))
                                                <span id="cpf-error" class="error text-danger"
                                                      for="input-cpf">{{ $errors->first('cpf') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Número de identidade') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('identity_number') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('identity_number') ? ' is-invalid' : '' }}"
                                                   name="identity_number" id="input-identity_number" type="text"
                                                   placeholder="{{ __('Número de identidade') }}" value="{{ old('identity_number', $vet->identity_number) }}" required/>
                                            @if ($errors->has('identity_number'))
                                                <span id="identity_number-error" class="error text-danger"
                                                      for="input-identity_number">{{ $errors->first('identity_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Registro no CFMV') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('board_registration') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('board_registration') ? ' is-invalid' : '' }}"
                                                   name="board_registration" id="input-board_registration" type="text"
                                                   placeholder="{{ __('Registro no CFMV') }}" value="{{ old('board_registration', $vet->board_registration) }}" required/>
                                            @if ($errors->has('board_registration'))
                                                <span id="board_registration-error" class="error text-danger"
                                                      for="input-board_registration">{{ $errors->first('board_registration') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label"
                                           for="input-pets-type">{{ __('Tipos de Pet') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <select class="form-control chosen-select" id="input-pets-type-id"
                                                    name="pets_type_id[]" required
                                                    data-placeholder="Escolha os tipos de Pet" multiple>
                                                <option id=""></option>
                                                @foreach (\App\PetTypes::all() as $petType)
                                                    <option value="{{ $petType->id }}"
                                                    @if($vet->petTypes->where('pet_type_id', $petType->id)->isNotEmpty()) selected @endif
                                                    >{{ $petType->label }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Salvar') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{ asset('js') }}/chosenjs/chosen.min.css" rel="stylesheet" />
@endpush

@push('js')
    <script src="{{ asset('js') }}/chosenjs/chosen.jquery.min.js"></script>
    <script type="application/javascript">
        $( document ).ready(function() {
            $(".chosen-select").chosen();
        });
    </script>
@endpush
