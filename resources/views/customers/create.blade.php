@extends('layouts.app', ['activePage' => 'customers', 'titlePage' => __('Criação de cliente')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('customer.store') }}" autocomplete="off" class="form-horizontal">
                        @csrf
                        @method('post')

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Novo cliente') }}</h4>
                                <p class="card-category"></p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <a href="{{ route('customer.index') }}"
                                           class="btn btn-sm btn-primary">{{ __('Voltar para lista') }}</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Nome') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Nome') }}" value="{{ old('name') }}"
                                                   required="true" aria-required="true"/>
                                            @if ($errors->has('name'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('name') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" id="input-email" type="email"
                                                   placeholder="{{ __('Email') }}" value="{{ old('email') }}" required/>
                                            @if ($errors->has('email'))
                                                <span id="email-error" class="error text-danger"
                                                      for="input-email">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('CPF') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('cpf') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}"
                                                   name="cpf" id="input-cpf" type="text" min="11" max="11"
                                                   placeholder="{{ __('CPF') }}" value="{{ old('cpf') }}" required/>
                                            @if ($errors->has('cpf'))
                                                <span id="cpf-error" class="error text-danger"
                                                      for="input-cpf">{{ $errors->first('cpf') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Número de identidade') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('identity_number') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('identity_number') ? ' is-invalid' : '' }}"
                                                   name="identity_number" id="input-identity_number" type="text"
                                                   placeholder="{{ __('Número de identidade') }}" value="{{ old('identity_number') }}" required/>
                                            @if ($errors->has('identity_number'))
                                                <span id="identity_number-error" class="error text-danger"
                                                      for="input-identity_number">{{ $errors->first('identity_number') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Pataz') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('pataz') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('pataz') ? ' is-invalid' : '' }}"
                                                   name="pataz" id="input-pataz" type="text"
                                                   placeholder="{{ __('Pataz') }}" value="{{ old('pataz') }}" required/>
                                            @if ($errors->has('pataz'))
                                                <span id="pataz-error" class="error text-danger"
                                                      for="input-pataz">{{ $errors->first('pataz') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label"
                                           for="input-password">{{ __(' Senha') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                            <input
                                                class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                input type="password" name="password" id="input-password"
                                                placeholder="{{ __('Senha') }}" value="" required/>
                                            @if ($errors->has('password'))
                                                <span id="name-error" class="error text-danger"
                                                      for="input-name">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2 col-form-label"
                                           for="input-password-confirmation">{{ __('Confirmação de senha') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <input class="form-control" name="password_confirmation"
                                                   id="input-password-confirmation" type="password"
                                                   placeholder="{{ __('Confirmação de senha') }}" value="" required/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Criar usuário') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
